from django.contrib import admin
from django.urls import path, include
from front_end import views

urlpatterns = [
    #path('', views.show_index),
    #path('committee', views.show_index),
    path('', include('back_end.urls')),
    path('', views.show_construction)
]
