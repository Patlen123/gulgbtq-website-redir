from django.http import JsonResponse
from django.shortcuts import render, redirect
import requests
import json
import back_end.discord_bot_manager as bm

def show_construction(request):
    return render(request, 'under_construction.html')

def show_index(request):
    events = bm.get_events_as_list()
    announcements, avatars, guild_ids = bm.get_announcements()
    return render(request, 'index.html', {'events': events, 'announcements': announcements, 'avatars': avatars, 'guild_ids': guild_ids})
