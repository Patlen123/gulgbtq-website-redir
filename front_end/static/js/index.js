function callSendInviteRequest() {
  var username = prompt("Please enter your Discord username (make sure that you are accepting friend requests):");
  if (username !== null) {
    $.ajax({
      url: '/discord/invite',
      type: 'GET',
      data: { username: username },
      success: function(data) {
        console.log(data.message);
      },
      error: function(xhr, status, error) {
        console.error(error);
      }
    });
    alert("Thanks, an admin will send you a friend request. Once you accept this, they'll send you an invite link. (I know it's a hassle I'm working on a better solution)")
  }
}