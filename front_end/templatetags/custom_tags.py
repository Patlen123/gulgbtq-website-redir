from django import template
from dateutil import parser

register = template.Library()


@register.filter(name='to_date')
def to_date(value):
    return parser.isoparse(value)
