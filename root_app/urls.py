from django.contrib import admin
from django.urls import path, include

import front_end.views

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('', include('front_end.urls')),
    path('', include('back_end.urls')),
    path('', front_end.views.show_construction)
]
