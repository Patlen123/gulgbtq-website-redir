from django.db import models


class DiscordUser(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    username = models.CharField(max_length=255)
    avatar_hash = models.CharField(max_length=255)


class Announcement(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    author = models.ForeignKey(DiscordUser, on_delete=models.SET_NULL, null=True)
    content = models.TextField()
    timestamp = models.DateTimeField()
